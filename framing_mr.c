#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc,char **argv){
  FILE *ip1,*op1;
  char *inp1,*out1;
  int samplingfreq=16000;//Default values
  int windowsize=40;//in milliseconds and non-overlapping frames
  int i;
  int stepsize;
  int c;
  float x;

  while((c=getopt(argc,argv,"i:o:f:w:")) != -1)
    switch(c){
    case 'i':
      inp1=optarg;
      break;
    case 'o':
      out1=optarg;
      break;
    case 'f':
      samplingfreq=atoi(optarg);
      break;
    case 'w':
      windowsize=atoi(optarg);
      break;
    }
  stepsize=(samplingfreq/1000)*windowsize;
  ip1=fopen(inp1,"r");
  op1=fopen(out1,"w");
  i=0;
  while(!feof(ip1)){
    fscanf(ip1,"%f",&x);
    i++;
    fprintf(op1,"%f ",x);
    if(i>=stepsize){
      fprintf(op1,"\n");
      i=0;
    }
  }
  if(i > 0){
    while(i<stepsize){
      fprintf(op1,"0 ");
      i++;
    }
    fprintf(op1,"\n");
  }
  fclose(ip1);
  fclose(op1);
  return 0;
}
