#!/bin/perl

use warnings;
use strict;

if($#ARGV < 0){
    print "\nStacks mceps together so that 40ms frames can be formed by stacking 4 10ms frames etc...\nUsage:\n1.Input file\n2:Input file frame size\n3:Output file frame size\n\nNote:\nFrames will be non-overlapping\n\n";
    exit;
}

open(Input,"<$ARGV[0]");
my $inp_fs=$ARGV[1];
my $out_fs=$ARGV[2];

my $stepsize=$out_fs/$inp_fs;
my $step=0;
my $x;

while(<Input>){
    chomp;
    $x=$_;
    print "$x ";
    $step++;
    if($step >= $stepsize){
	$step=0;
	print "\n";
    }
}
if($step!=0){
    while($step < $stepsize){
	print "$x ";
	$step++;
    }
    print "\n";
}
close(Input);
