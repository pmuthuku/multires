# Makefile for Multiresolution code

OBJS= n_decimator
CC = gcc
CFLAGS = -lm -g -Wall

all : n_decimator framing_mr

n_decimator : n_decimator.c
	$(CC) n_decimator.c $(CFLAGS) -o n_decimator

framing_mr : framing_mr.c
	$(CC) framing_mr.c $(CFLAGS) -o framing_mr


