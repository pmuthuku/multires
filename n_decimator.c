#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

int main(int argc,char **argv){
  FILE *ip1,*op1;
  char *inp1,*out1;
  float x;
  int n;
  int i;
  int c;
  while((c=getopt(argc,argv,"i:o:n:")) != -1)
    switch(c){
    case 'i':
      inp1=optarg;
      break;
    case 'o':
      out1=optarg;
      break;
    case 'n':
      n=atoi(optarg);
      break;
    }
  ip1=fopen(inp1,"r");
  op1=fopen(out1,"w");
  while(!feof(ip1)){
    fscanf(ip1,"%f",&x);
    fprintf(op1,"%f\n",x);
    for(i=0;i < (n-1); i++){
      fscanf(ip1,"%f",&x);
    }
  }
  fclose(ip1);
  fclose(op1);
  return 0;
}
