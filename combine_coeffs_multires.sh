#!/bin/bash

PROMPTFILE=etc/txt.done.data

if [ ! -d ccoefs ]
then
    mkdir ccoefs
fi
CG_TMP=cg_tmp_$$

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do
    fname=$i
    echo $fname "COMBINE_COEFFS (f0,mceps,mceps_level1,mceps_level2,v)"

    if [ -f festival/utts/$fname.utt ]
    then
	enddur=`$ESTDIR/../festival/examples/dumpfeats -relation Segment -feats '(end)' festival/utts/$fname.utt | tail -1 | awk '{printf("%0.3f",$1+0.0005)}'`
    else
	enddur=`$ESTDIR/bin/ch_track -otype est_ascii mcep_deltas/$fname.mcep | awk '{time=$1} END {printf("%0.3f",time)}'`
    fi

    $ESTDIR/bin/ch_track -otype ascii f0/$fname.f0 |
      awk '{if (NR == 1) { print $1; print $1} print $1}' >$CG_TMP.f0

    # $ESTDIR/bin/ch_track -otype ascii mcep/$fname.mcep |
    #   awk '{if (NR == 1) { print $0; } print $0}' >$CG_TMP.mcep1

    # $ESTDIR/bin/ch_track -otype ascii mcep/$fname.d1mcep |
    #   awk '{if (NR == 1) { print $0; } print $0}' >$CG_TMP.mcep2

    # $ESTDIR/bin/ch_track -otype ascii mcep/$fname.d2mcep |
    #   awk '{if (NR == 1) { print $0; } print $0}' >$CG_TMP.mcep3

    cat v/$fname.v | awk '{print 10*$1}' >$CG_TMP.v

 #   paste $CG_TMP.f0 $CG_TMP.mcep1 $CG_TMP.mcep2 $CG_TMP.mcep3 $CG_TMP.v |
    paste $CG_TMP.f0 mcep/$fname.mcep mcep/$fname.d1mcep mcep/$fname.d2mcep $CG_TMP.v |
    awk '{if (l==0) 
              l=NF;
            else if (l == NF)
              print $0}' |
    awk '{if (NR == -1) print $0; print $0}' |
    $ESTDIR/bin/ch_track -itype ascii -otype est_binary -s 0.005 -end $enddur -o ccoefs/$fname.mcep
    rm -f $CG_TMP.*

done